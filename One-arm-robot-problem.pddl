(define (problem one-arm-robot_problem)
    (:domain one-arm-robot_domain)
    (:objects 
        pos1    pos2    pos3    pos4    pos5    pos6
        red     blue    green   yellow  
        bot
    )
    (:init
        (at yellow pos1)        (at blue pos2)        (at green pos4)        (at red pos5)
        (empty pos3)        (empty pos6)
        (at bot pos1)

        (gripper bot)
        (pos pos1)      (pos pos2)      (pos pos3)      (pos pos4)      (pos pos5)      (pos pos6)
        (cup red)       (cup green)     (cup blue)      (cup yellow)
    )
    (:goal (and
        (at yellow pos3)
        (at green pos4)
        (at blue pos5)
        (at red pos6)
        (empty pos1)
        (empty pos2))
    )
)